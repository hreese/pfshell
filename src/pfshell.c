#include <unistd.h>
#include <stdio.h>
#include <signal.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

int ignoreSigs[] = { SIGABRT, SIGQUIT, SIGTRAP };

void signalHandlerExit(int sig_num) {
	_exit(0);
}

int main(int argc, char const *argv[]) {
	// ignore a few signals
	struct sigaction new_action;
	for (int i=0; i < sizeof(ignoreSigs) / sizeof(int); i++) {
		new_action.sa_handler = signalHandlerExit;
		sigemptyset (&new_action.sa_mask);
		new_action.sa_flags = 0;
		sigaction(i, &new_action, NULL);
	}

	printf(ANSI_COLOR_RED "ssh port forwarding only" ANSI_COLOR_RESET "\n");

	for(;;) pause();
}

