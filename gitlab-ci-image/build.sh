#!/bin/bash
# shellcheck disable=SC1004

set -e

# docker build -t registry.gitlab.com/hreese/pfshell .
# docker push registry.gitlab.com/hreese/pfshell
BASENAME=pfshell
TAG='latest'
REGISTRY='registry.gitlab.com/hreese/pfshell'
USERNAME='nobody'
PASSWORD='password'

while getopts "r:u:p:b:t:" OPTION; do
    case $OPTION in
        r)
            REGISTRY="${OPTARG}"
            ;;
        u)
            USERNAME="${OPTARG}"
            ;;
        p)
            PASSWORD="${OPTARG}"
            ;;
        b)
            BASENAME="${OPTARG}"
            ;;
        t)
            TAG="${OPTARG}"
            ;;
        *)
            echo "Usage: $0 [OPTIONS]"
            echo
            echo "Options:"
            echo "  -r <registry> (default: \"${REGISTRY}\")"
            echo "  -u <username> (default: \"${USERNAME}\")"
            echo "  -p <password> (default: \"${PASSWORD}\")"
            echo "  -b <basename> (default: \"${BASENAME}\")"
            echo "  -t <tag>      (default: \"${TAG}\")"
            echo
            exit 127
    esac
done

REGHOST="$(echo "${REGISTRY}" | cut -d/ -f1)"
if [ "${REGISTRY}" != "none" ]; then
        echo "🔑 Logging into »${REGHOST}«"
        if ! buildah login --get-login "${REGHOST}" > /dev/null 2> /dev/null; then
                buildah login --password "${PASSWORD}" --username "${USERNAME}" "${REGHOST}"
        fi
fi

buildah build-using-dockerfile \
	--from debian:latest \
	--format docker \
	--tag "${REGISTRY}" \
	--build-arg BUILDTIMESTAMP="`date --iso-8601=seconds`"

if [ "${REGISTRY}" != "none" ]; then
	echo "🚀 Pushing »${IMAGETAG}« to »${REGISTRY}«"
	buildah push "${REGISTRY}"
fi

