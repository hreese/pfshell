# pfshell

Minimal shell-like program that does not close ssh connections but keeps them open indefinitely. Use case: port forwarding only user accounts.

## Build and install

With meson:

```sh
meson setup build
cd build
sudo meson install
```

Just gcc and coreutils:

```sh
cd src
gcc pfshell.c -o pfshell
sudo install -s -v pfshell /usr/local/bin/
```

## Usage

Add `pfshell` to list of allowed shells:
```sh
grep -q /usr/local/bin/pfshell /etc/shells || sudo echo /usr/local/bin/pfshell >> /etc/shells
```

Create user `portforwarding`:
```sh
useradd --system --create-home --shell /usr/local/bin/pfshell --comment "SSH port forwarding only" portforwarding
```

Add ssh config:
```sh
sudo mkdir -m 0700 -p ~portforwarding/.ssh
sudo touch ~portforwarding/.ssh/authorized_keys
sudo chmod 600 ~portforwarding/.ssh/authorized_keys
sudo chown -R portforwarding: ~portforwarding/.ssh
```

Now add all relevant ssh public keys to `~portforwarding/.ssh/authorized_keys`.

Use something like [autossh](https://github.com/Autossh/autossh) to keep remote port forwarding sessions alive.
